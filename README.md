# antares

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


# Users de teste

### Administrador
E-mail: adm_antares@hotmail.com
Senha: administração#1234

### Logística
E-mail: logistica_antares@hotmail.com
Senha: logistica#1234

### Distribuidor
E-mail: jean@meeg.app
Senha: distribuidor#1234

### Super admin
E-mail: admin@meeg.app
Senha: admin#1234
